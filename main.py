import numpy as np
import cv2
import cvlib as cv
import pygame
import playsound
import tensorflow as tf

cap = cv2.VideoCapture(0)

# model = tf.keras.models.load_model()


j = 0

pygame.mixer.init()
pygame.mixer.music.load('bonjour.mp3')

seen = False

while True:
    j += 1
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

    # Capture frame-by-frame
    ret, frame = cap.read()

    faces, confidences = cv.detect_face(frame, 0.9)
    # loop through detected faces and add bounding box

    i = 0

    if len(faces) == 0:
        seen = False

    for face in faces:
        i += 1

        try:
            (startX, startY) = face[0], face[1]
            (endX, endY) = face[2], face[3]

            # draw rectangle over face
            cv2.rectangle(frame, (startX, startY), (endX, endY), (0, 255, 0), 2)

            # cv2.imwrite('img/test-{0}.png'.format(j), frame[startY:endY,startX:endX])
            cv2.imshow('face-{0}'.format(i), frame[startY:endY, startX:endX])

            if not seen and not pygame.mixer.music.get_busy():
                pygame.mixer.music.play(0)

            seen = True
        except:
            pass

    # Display the resulting frame
    cv2.imshow('frame', frame)

cap.release()
cv2.destroyAllWindows()
